<section id="slider">
  <!--slider-->
  <div class="container">
    <div class="row">
      <div class="col-md-13">
        <img src="img/ban.png" class="img-thumbnail" alt="ban gambar">
      </div>
      <div class="col-md-6">
      </div>
    </div>
  </div>
</section>
<!--/slider-->
<section>
  <div class="container">
    <div class="row">
      <div class="col-sm-3">
        <?php include 'sidebar.php'; ?>
      </div>
      <div class="col-sm-9 padding-right">
        <div class="features_items">
          <!--features_items-->
          <h2 class="title text-center" style="padding-top:10px;">Produk</h2>
          <?php
          $query = "SELECT * FROM `tblpromopro` pr , `tblproduct` p , `tblcategory` c
          WHERE pr.`PROID`=p.`PROID` AND  p.`CATEGID` = c.`CATEGID`  AND PROQTY>0 ";
          $mydb->setQuery($query);
          $cur = $mydb->loadResultList();
          foreach ($cur as $result) {
          ?>
          <form method="POST" action="cart/controller.php?action=add">
            <input type="hidden" name="PROPRICE" value="<?php echo $result->PROPRICE; ?>">
            <input type="hidden" name="PROQTY" value="<?php echo $result->PROQTY; ?>">
            <input type="hidden" name="PROID" value="<?php echo $result->PROID; ?>">
            <div class="col-sm-4">
              <div class="product-image-wrapper">
                <div class="single-products">
                  <div class="productinfo text-center">
                    <img src="<?php echo web_root . 'admin/products/' . $result->IMAGES; ?>" alt="" />
                    <h2>Rp. <?php echo number_format($result->PRODISPRICE, 2); ?></h2>
                    <p><?php echo $result->OWNERNAME; ?></p>
                  </div>
                  <div class="product-overlay">
                    <div class="overlay-content">
                      <h2>Rp. <?php echo number_format($result->PRODISPRICE, 2); ?></h2>
                      <p><?php echo $result->OWNERNAME; ?></p>
                      <button type="submit" name="btnorder" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Beli</button>
                      <button type="button" data-toggle="modal" data-target="#exampleModal<?=$result->PROID?>" class="btn btn-danger" style="margin-bottom: 25px;">Lihat Detail</button>
                    </div>
                  </div>
                  <?php include 'detailProdukModal.php'; ?>
                </div>
                <div class="choose">
                  <ul class="nav nav-pills nav-justified">
                    <li>
                      <?php
                      if (isset($_SESSION['CUSID'])) {
                      echo ' <a href="' . web_root . 'customer/controller.php?action=addwish&proid=' . $result->PROID . '" title="Add to wishlist"><i class="fa fa-plus-square"></i>Add to wishlist</a></a>
                      ';
                      } else {
                      echo   '<a href="#" title="Add to wishlist" class="proid"  data-target="#smyModal" data-toggle="modal" data-id="' .  $result->PROID . '"><i class="fa fa-plus-square"></i>Add to wishlist</a></a>
                      ';
                      }
                      ?>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </form>
          <?php  } ?>
        </div>
        <!--features_items-->
        <div class="recommended_items">
          <!--recommended_items-->
          <h2 class="title text-center">Produk Terlaris</h2>
          <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
              <div class="item active">
                <?php
                $query = "SELECT * FROM `tblpromopro` pr , `tblproduct` p , `tblcategory` c
                WHERE pr.`PROID`=p.`PROID` AND  p.`CATEGID` = c.`CATEGID`  AND PROQTY>0 limit 3 ";
                $mydb->setQuery($query);
                $cur = $mydb->loadResultList();
                foreach ($cur as $result) {
                ?>
                <form method="POST" action="cart/controller.php?action=add">
                  <input type="hidden" name="PROPRICE" value="<?php echo $result->PROPRICE; ?>">
                  <input type="hidden" name="PROQTY" value="<?php echo $result->PROQTY; ?>">
                  <input type="hidden" name="PROID" value="<?php echo $result->PROID; ?>">
                  <div class="col-sm-4">
                    <div class="product-image-wrapper">
                      <div class="single-products">
                        <div class="productinfo text-center">
                          <img src="<?php echo web_root . 'admin/products/' . $result->IMAGES; ?>" alt="" />
                          <h2>Rp. <?php echo number_format($result->PRODISPRICE, 2); ?></h2>
                          <p><?php echo $result->OWNERNAME; ?></p>
                          <button type="submit" name="btnorder" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Beli</button>
                          <button type="button" data-toggle="modal" data-target="#exampleModal<?=$result->PROID?>" class="btn btn-danger" style="margin-bottom: 25px;">Lihat Detail</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
                <?php } ?>
              </div>
              <div class="item">
                <?php
                $query = "SELECT * FROM `tblpromopro` pr , `tblproduct` p , `tblcategory` c
                WHERE pr.`PROID`=p.`PROID` AND  p.`CATEGID` = c.`CATEGID`  AND PROQTY>0 limit 3,6";
                $mydb->setQuery($query);
                $cur = $mydb->loadResultList();
                foreach ($cur as $result) {
                ?>
                <form method="POST" action="cart/controller.php?action=add">
                  <input type="hidden" name="PROPRICE" value="<?php echo $result->PROPRICE; ?>">
                  <input type="hidden" id="PROQTY" name="PROQTY" value="<?php echo $result->PROQTY; ?>">
                  <input type="hidden" name="PROID" value="<?php echo $result->PROID; ?>">
                  <div class="col-sm-4">
                    <div class="product-image-wrapper">
                      <div class="single-products">
                        <div class="productinfo text-center">
                          <img src="<?php echo web_root . 'admin/products/' . $result->IMAGES; ?>" alt="" />
                          <h2>Rp.<?php echo number_format($result->PRODISPRICE, 2); ?></h2>
                          <p><?php echo $result->OWNERNAME; ?></p>
                          <button type="submit" name="btnorder" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Beli</button>
                          <button type="button" data-toggle="modal" data-target="#exampleModal<?=$result->PROID?>" class="btn btn-danger" style="margin-bottom: 25px;">Lihat Detail</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </form>
                <?php } ?>
              </div>
            </div>
            <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
              <i class="fa fa-angle-left"></i>
            </a>
            <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
              <i class="fa fa-angle-right"></i>
            </a>
          </div>
        </div>
        <!--/recommended_items-->
      </div>
    </div>
  </div>
  <div class="container">
    <div class="row">
      <div class="col-md-5">
        <div class="panel panel-primary">
          <div class="panel-heading" id="accordion">
            <span class="glyphicon glyphicon-comment"></span> Chat
            <div class="btn-group pull-right">
              <a type="button" class="btn btn-default btn-xs" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                <span class="glyphicon glyphicon-chevron-down"></span>
              </a>
            </div>
          </div>
          <div class="panel-collapse collapse" id="collapseOne">
            <div class="panel-body">
              <ul class="chat">
                <li class="left clearfix"><span class="chat-img pull-left">
                  <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
                </span>
                <div class="chat-body clearfix">
                  <div class="header">
                    <strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">
                    <span class="glyphicon glyphicon-time"></span>12 mins ago</small>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                    dolor, quis ullamcorper ligula sodales.
                  </p>
                </div>
              </li>
              <li class="right clearfix"><span class="chat-img pull-right">
                <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
              </span>
              <div class="chat-body clearfix">
                <div class="header">
                  <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>13 mins ago</small>
                  <strong class="pull-right primary-font">Bhaumik Patel</strong>
                </div>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                  dolor, quis ullamcorper ligula sodales.
                </p>
              </div>
            </li>
            <li class="left clearfix"><span class="chat-img pull-left">
              <img src="http://placehold.it/50/55C1E7/fff&text=U" alt="User Avatar" class="img-circle" />
            </span>
            <div class="chat-body clearfix">
              <div class="header">
                <strong class="primary-font">Jack Sparrow</strong> <small class="pull-right text-muted">
                <span class="glyphicon glyphicon-time"></span>14 mins ago</small>
              </div>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
                dolor, quis ullamcorper ligula sodales.
              </p>
            </div>
          </li>
          <li class="right clearfix"><span class="chat-img pull-right">
            <img src="http://placehold.it/50/FA6F57/fff&text=ME" alt="User Avatar" class="img-circle" />
          </span>
          <div class="chat-body clearfix">
            <div class="header">
              <small class=" text-muted"><span class="glyphicon glyphicon-time"></span>15 mins ago</small>
              <strong class="pull-right primary-font">Bhaumik Patel</strong>
            </div>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur bibendum ornare
              dolor, quis ullamcorper ligula sodales.
            </p>
          </div>
        </li>
      </ul>
    </div>
    <div class="panel-footer">
      <div class="input-group">
        <input id="btn-input" type="text" class="form-control input-sm" placeholder="Type your message here..." />
        <span class="input-group-btn">
          <button class="btn btn-warning btn-sm" id="btn-chat">
          Send</button>
        </span>
      </div>
    </div>
  </div>
</div>
</div>
</div>
</div>
</section>