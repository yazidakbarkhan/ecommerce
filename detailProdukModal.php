<div class="modal fade" id="exampleModal<?=$result->PROID?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content" style="width: 800px;">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Detail Produk</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-sm-6">
            <img src="<?= web_root . 'admin/products/' . $result->IMAGES; ?>" width="400" alt="">
          </div>
          <div class="col-sm-6">
            <table class="table table-borderless">
              <tbody>
                <tr>
                  <th scope="col">Nama Produk</th>
                  <td><?= $result-> OWNERNAME; ?></td>
                </tr>
                <tr>
                  <th scope="col">Merk/Type</th>
                  <td><?= $result-> CATEGID; ?></td>
                </tr>
                <tr>
                  <th scope="col">Stok</th>
                  <td><?= $result-> PROQTY; ?></td>
                </tr>
                <!-- <tr>
                  <th scope="">Warna</th>
                  <td>
                    <div class="radio">
                      <label>
                        <input type="radio" name="optionsRadios" id="warna1" value="option1" checked>
                        Kuning
                      </label>
                    </div>
                    <div class="radio">
                      <label>
                        <input type="radio" name="optionsRadios" id="warna2" value="option2">
                        Merah
                      </label>
                    </div>
                    <div class="radio disabled">
                      <label>
                        <input type="radio" name="optionsRadios" id="warna3" value="option3">
                        Biru
                      </label>
                    </div>
                  </td>
                </tr> -->
                <tr>
                  <th scope="col">Deskripsi</th>
                  <td><?= $result->PRODESC; ?></td>
                </tr>
                <tr>
                  <th scope="col">Harga</th>
                  <td>Rp.<?= number_format($result->PRODISPRICE, 2) ?></td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
        <button type="submit" name="btnorder" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i> Beli Sekarang</button>
      </div>
    </div>
  </div>
</div>