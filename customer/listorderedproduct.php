<?php
require_once ("../include/initialize.php");
if (!isset($_SESSION['CUSID'])){
redirect("index.php");
}
	
// if (isset($_POST['id'])){
// if ($_POST['actions']=='confirm') {
							// 							# code...
			// 	$status	= 'Confirmed';
	// 	// $remarks ='Your order has been confirmed. The ordered products will be yours anytime.';
	
// }elseif ($_POST['actions']=='cancel'){
	// 	// $order = New Order();
		// 	$status	= 'Cancelled';
	// 	// $remarks ='Your order has been cancelled due to lack of communication and incomplete information.';
// }
// $summary = New Summary();
// $summary->ORDEREDSTATS     = $status;
// $summary->update($_POST['id']);
// }
if(isset($_POST['close'])){
	unset($_SESSION['ordernumber']);
	redirect(web_root.'index.php');
}
if (isset($_POST['ordernumber'])){
	$_SESSION['ordernumber'] = $_POST['ordernumber'];
}
// unsetting notify msg
$summary = New Summary();
$summary->HVIEW = 1;
$summary->update($_SESSION['ordernumber']);
// end
$query = "SELECT * FROM `tblsummary` s ,`tblcustomer` c
		WHERE   s.`CUSTOMERID`=c.`CUSTOMERID` and ORDEREDNUM='".$_SESSION['ordernumber']."'";
		$mydb->setQuery($query);
		$cur = $mydb->loadSingleResult();
// $query = "SELECT * FROM tblusers
				// 				WHERE   `USERID`='".$_SESSION['cus_id']."'";
		// 		$mydb->setQuery($query);
		// 		$row = $mydb->loadSingleResult();
?>

<div class="modal-dialog" style="width:60%">
	<div class="modal-content">
		<div class="modal-header">
			<button class="close" id="btnclose" data-dismiss="modal" type= "button">×</button>
			<span id="printout">
				
				<table>
					<tr>
						<td align="center">
							<img src="<?php echo web_root; ?>images/home/logo.png"   alt="Image">
						</td>
					</tr>
				</table>
				<!-- <h2 class="modal-title" id="myModalLabel">Billing Details </h2> -->
				
				
				<div class="modal-body">
					<?php
						$query = "SELECT * FROM `tblsummary` s ,`tblcustomer` c
									WHERE   s.`CUSTOMERID`=c.`CUSTOMERID` and ORDEREDNUM=".$_SESSION['ordernumber'];
							$mydb->setQuery($query);
							$cur = $mydb->loadSingleResult();
							if($cur->ORDEREDSTATS=='Confirmed'){
							
							if ($cur->PAYMENTMETHOD=="Cash on Pickup") {
								
							
					?>
					<h4>Pesananmu telah di konfirmasi</h4><br/>
					<h5>Dengan Hormat</h5>
					<h5>Karena Anda telah memesan uang tunai saat pengambilan, harap minta jumlah uang tunai yang tepat untuk dibayarkan kepada staf kami dan bawalah detail penagihan ini.</h5>
					<hr/>
					<h4><strong>Pick up Information</strong></h4>
					<div class="row">
						<!-- <div class="col-md-6">
							<p> ORDER NUMBER : <?php echo $_SESSION['ordernumber']; ?></p>
							<?php
								$query="SELECT sum(ORDEREDQTY) as 'countitem' FROM `tblorder` WHERE `ORDEREDNUM`='".$_SESSION['ordernumber']."'";
								$mydb->setQuery($query);
								$res = $mydb->loadResultList();
							?>
							<p>Items to be pickup : <?php
							foreach ( $res as $row) echo $row->countitem; ?></p>
						</div> -->
						<div class="col-md-6">
							<p>Name : <?php echo $cur->FNAME . ' '.  $cur->LNAME ;?></p>
							<p>Address : <?php echo $cur->CUSHOMENUM . ' ' . $cur->STREETADD . ' ' .$cur->BRGYADD . ' ' . $cur->CITYADD . ' ' .$cur->PROVINCE . ' ' .$cur->COUNTRY; ?></p>
							<!-- <p>Contact Number : <?php echo $cur->CONTACTNUMBER;?></p> -->
						</div>
					</div>
					<?php
					}elseif ($cur->PAYMENTMETHOD=="Cash on Delivery"){
							
					?>
					<h4>Pesanan Anda telah dikonfirmasi dan dikirim</h4><br/>
					<h5>Dengan Hormat</h5>
					<h5>Pesanan Anda sedang dalam perjalanan! Seperti yang telah Anda pesan melalui Cash on Delivery, harap miliki jumlah uang tunai yang tepat untuk pengirim kami.	</h5>
					<hr/>
					<h4><strong>Delivery Information</strong></h4>
					<div class="row">
						<div class="col-md-6">
							<p> ORDER NUMBER : <?php echo $_SESSION['ordernumber']; ?></p>
							<?php
							$query="SELECT sum(ORDEREDQTY) as 'countitem' FROM `tblorder` WHERE `ORDEREDNUM`='".$_SESSION['ordernumber']."'";
							$mydb->setQuery($query);
							$res = $mydb->loadResultList();
							?>
							<p>Barang yang akan dikirim : <?php
							foreach ( $res as $row) echo $row->countitem; ?></p>
						</div>
						<div class="col-md-6">
							<p>Nama : <?php echo $cur->FNAME . ' '.  $cur->LNAME ;?></p>
							<!-- <p>Address : <?php echo $cur->ADDRESS;?></p> -->
							<!-- <p>Contact Number : <?php echo $cur->CONTACTNUMBER;?></p> -->
						</div>
					</div>
					<?php
					}
					}elseif($cur->ORDEREDSTATS=='Cancelled'){
						echo "Pesanan Anda telah dibatalkan karena kurangnya komunikasi dan informasi yang tidak lengkap.";
					}else{
						echo "<h5>Pesanan Anda sedang dalam proses. Silakan periksa profil Anda untuk pemberitahuan konfirmasi.</h5>";
					}
					?>
					<hr/>
					<h4><strong>Order Information</strong></h4>
					<table id="table" class="table">
						<thead>
							<tr>
								<!-- <th>PRODUCT</th>? -->
								<th>Produk</th>
								<!-- <th>DATE ORDER</th>  -->
								<th>Harga</th>
								<th>QUANTITY</th>
								<th>Total Harga</th>
								<th>Rating</th>
							</tr>
						</thead>
						<tbody>
							
							<?php
							$subtot=0;
							$query = "SELECT *
										FROM  `tblproduct` p, tblcategory ct,  `tblcustomer` c,  `tblorder` o,  `tblsummary` s
										WHERE p.`CATEGID` = ct.`CATEGID`
										AND p.`PROID` = o.`PROID`
										AND o.`ORDEREDNUM` = s.`ORDEREDNUM`
										AND s.`CUSTOMERID` = c.`CUSTOMERID`
										AND o.`ORDEREDNUM`=".$_SESSION['ordernumber'];
							$mydb->setQuery($query);
							$cur = $mydb->loadResultList();
							foreach ($cur as $result) {
								echo '<tr>';
								// echo '<td ><img src="'.web_root.'admin/modules/product/'. $result->IMAGES.'" width="60px" height="60px" title="'.$result->PRODUCTNAME.'"/></td>';
								// echo '<td>' . $result->PRODUCTNAME.'</td>';
								// echo '<td>'. $result->FIRSTNAME.' '. $result->LASTNAME.'</td>';
								echo '<td>'.$result->PRODESC
								 .'<span id=`ratingParent'.$result->PROID.'`> </span></td>';
								// echo '<td>'.date_format(date_create($result->ORDEREDDATE),"M/d/Y h:i:s").'</td>';
								echo '<td> &#8369 '. number_format($result->PROPRICE,2).' </td>';
								echo '<td align="center" >'. $result->ORDEREDQTY.'</td>';
								?>
								<td>Rp<output class="output"><?php echo  number_format($result->ORDEREDPRICE,2); ?></output></td>
								<?php
								if ($result->RATING) {
									echo'<td align="center" > '.$result->RATING.'</td>';
								}else{
									echo '<td align="center" >
											<button 
											data-order='.$_SESSION['ordernumber'].'
											data-id='.$result->PROID.'
											class="rate">rate</button>
										</td>';
									// echo '<td align="center" > '.$_SESSION['ordernumber'].'-'.$result->PROID.'</td>';
								}
								echo '</tr>';
								$subtot +=$result->ORDEREDPRICE;
							
							}
							?>
						</tbody>
						<tfoot >
						<?php
								$query = "SELECT * FROM `tblsummary` s ,`tblcustomer` c
								WHERE   s.`CUSTOMERID`=c.`CUSTOMERID` and ORDEREDNUM=".$_SESSION['ordernumber'];
						$mydb->setQuery($query);
						$cur = $mydb->loadSingleResult();
						if ($cur->PAYMENTMETHOD=="Cash on Delivery") {
							# code...
							$price = $cur->DELFEE;
						}else{
							$price = 0.00;
						}
						// $tot =   $cur->PAYMENT  + $price;
						?>
						</tfoot>
					</table> <hr/>
					<div class="row">
						<div class="col-md-6 pull-left">
							<div>Tanggal Order: <?php echo date_format(date_create($cur->ORDEREDDATE),"M/d/Y h:i:s"); ?></div>
							<div>Metode Pembayaran: <?php echo $cur->PAYMENTMETHOD; ?></div>
						</div>
						<div class="col-md-6 pull-right">
							<p align="right">Total Price : Rp. <?php echo number_format($subtot,2);?></p>
							<p align="right">Delivery Fee :Rp.<?php echo number_format($price,2); ?></p>
							<p align="right">Overall Price :Rp. <?php echo number_format($cur->PAYMENT,2); ?></p>
						</div>
					</div>
					
					<?php
					if($cur->ORDEREDSTATS=="Confirmed"){
					?>
					<hr/>
					<div class="row">
						<p>Harap cetak ini sebagai bukti pembelian</p><br/>
						<p>Kami harap Anda menikmati produk yang Anda beli. Semoga harimu menyenangkan!</p>
						<p>Sincerely.</p>
						<h4><a href="https://bit.ly/2LPn9Wu">Ricollection</a></h4>
					</div>
					<?php }?>
				</div>
			</span>
			<div class="modal-footer">
				<div id="divButtons" name="divButtons">
					<?php if($cur->ORDEREDSTATS!='Pending' || $cur->ORDEREDSTATS!='Cancelled' ){ ?>
					
					<button  onclick="tablePrint();" class="btn btn_fixnmix pull-right "><span class="glyphicon glyphicon-print" ></span> Print</button>
					
					<?php } ?>
					<button class="btn btn-pup" id="btnclose" data-dismiss="modal" type=
					"button">Close</button>
				</div>
				<!-- <button class="btn btn-primary"
				name="savephoto" type="submit">Upload Photo</button> -->
			</div>
			<!-- </form> -->
			</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content" style="width: 800px;">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Beri Rating Produk</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
				<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="customer/controller.php?action=rating" method="post">
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
							<header class='header text-center'>
								<h5>Rating Produk</h5>
							</header>
							<section class='rating-widget'>
								
								<!-- Rating Stars Box -->
								<div class='rating-stars text-center'>
									<ul id='stars'>
										<li class='star' title='Buruk' data-value='1'>
											<i class='fa fa-star fa-fw'></i>
										</li>
										<li class='star' title='Sedang' data-value='2'>
											<i class='fa fa-star fa-fw'></i>
										</li>
										<li class='star' title='Bagus' data-value='3'>
											<i class='fa fa-star fa-fw'></i>
										</li>
										<li class='star' title='Sempurna' data-value='4'>
											<i class='fa fa-star fa-fw'></i>
										</li>
										<li class='star' title='Sangat Sempurna' data-value='5'>
											<i class='fa fa-star fa-fw'></i>
										</li>
									</ul>
								</div>
							</section>
						</div>
					</div>
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
								<input type="hidden" name="rating" value="5">
								<label><h5>Komentar Produk :</h5></label>
								<textarea name="komentar" class="form-control" rows="5"></textarea>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal">Kembali</button>
					<button type="submit" class="btn btn-primary" style="margin-bottom: 18px">Simpan</button>
				</div>
			</form>
		</div>
	</div>
</div>
		
<script type="text/javascript">
$(document).ready(function(){
	console.log('jalan')
	$('.rate').on('click',function (argument) {
		console.log($(this), 'isoo')
	})
	/* 1. Visualizing things on Hover - See next part for action on click */
	$('#stars li').on('mouseover', function(){
	var onStar = parseInt($(this).data('value'), 10); // The star currently mouse on

	// Now highlight all the stars that's not after the current hovered star
	$(this).parent().children('li.star').each(function(e){
	if (e < onStar) {
	$(this).addClass('hover');
	}
	else {
	$(this).removeClass('hover');
	}
	});

	}).on('mouseout', function(){
	$(this).parent().children('li.star').each(function(e){
	$(this).removeClass('hover');
	});
	});


	/* 2. Action to perform on click */
	$('#stars li').on('click', function(){
	var onStar = parseInt($(this).data('value'), 10); // The star currently selected
	var stars = $(this).parent().children('li.star');

	for (i = 0; i < stars.length; i++) {
	$(stars[i]).removeClass('selected');
	}

	for (i = 0; i < onStar; i++) {
	$(stars[i]).addClass('selected');
	}

	// JUST RESPONSE (Not needed)
	var ratingValue = parseInt($('#stars li.selected').last().data('value'), 10);
	var msg = "";
	if (ratingValue > 1) {
	msg = "Thanks! You rated this " + ratingValue + " stars.";
	}
	else {
	msg = "We will improve ourselves. You rated this " + ratingValue + " stars.";
	}
	console.log(msg, ratingValue)

	});
});
</script>
<script>
function tablePrint(){
	// document.all.divButtons.style.visibility = 'hidden';
	var display_setting="toolbar=no,location=no,directories=no,menubar=no,";
	display_setting+="scrollbars=no,width=500, height=500, left=100, top=25";
	var content_innerhtml = document.getElementById("printout").innerHTML;
	var document_print=window.open("","",display_setting);
	document_print.document.open();
	document_print.document.write('<body style="font-family:verdana; font-size:12px;" onLoad="self.print();self.close();" >');
		document_print.document.write(content_innerhtml);
	document_print.document.write('</body></html>');
	document_print.print();
	document_print.document.close();
	// document.all.divButtons.style.visibility = 'Show';

	return false;
}

</script>