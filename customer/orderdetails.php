<?php
if (!isset($_SESSION['CUSID'])){
redirect(web_root."index.php");
}
$customerid =$_SESSION['CUSID'];
$customer = New Customer();
$singlecustomer = $customer->single_customer($customerid);
?>
<?php
$autonumber = New Autonumber();
$res = $autonumber->set_autonumber('ordernumber');
?>
<form onsubmit="return orderfilter()" action="customer/controller.php?action=processorder" method="post" >
  <section id="cart_items">
    <div class="container">
      <div class="breadcrumbs">
        <ol class="breadcrumb">
          <li><a href="#">Home</a></li>
          <li class="active">Order Details</li>
        </ol>
      </div>
      <div class="row">
        <div class="col-md-6 pull-left">
          <div class="col-md-2 col-lg-2 col-sm-2" style="float:left">
            Name:
          </div>
          <div class="col-md-8 col-lg-10 col-sm-3" style="float:left">
            <?php echo $singlecustomer->FNAME .' '.$singlecustomer->LNAME; ?>
          </div>
          <div class="col-md-2 col-lg-2 col-sm-2" style="float:left">
            Address:
          </div>
          <div class="col-md-8 col-lg-10 col-sm-3" style="float:left">
            <?php echo $singlecustomer->CUSHOMENUM . ' ' . $singlecustomer->STREETADD . ' ' .$singlecustomer->BRGYADD . ' ' . $singlecustomer->CITYADD . ' ' .$singlecustomer->PROVINCE . ' ' .$singlecustomer->COUNTRY; ?>
          </div>
        </div>
        <div class="col-md-6 pull-right">
          <div class="col-md-10 col-lg-12 col-sm-8">
            <input type="hidden" value="<?php echo $res->AUTO; ?>" id="ORDEREDNUM" name="ORDEREDNUM">
            Order Number :<?php echo $res->AUTO; ?>
          </div>
        </div>
      </div>
      <div class="table-responsive cart_info">
        
        <table class="table table-condensed" id="table">
          <thead >
            <tr class="cart_menu">
              <th style="width:12%; align:center; ">Product</th>
              <th >Description</th>
              <th style="width:15%; align:center; ">Quantity</th>
              <th style="width:15%; align:center; ">Price</th>
              <th style="width:15%; align:center; ">Total</th>
            </tr>
          </thead>
          <tbody>
            
            <?php
            $tot = 0;
            if (!empty($_SESSION['gcCart'])){
            $count_cart = @count($_SESSION['gcCart']);
            for ($i=0; $i < $count_cart  ; $i++) {
            $query = "SELECT * FROM `tblpromopro` pr , `tblproduct` p , `tblcategory` c
            WHERE pr.`PROID`=p.`PROID` AND  p.`CATEGID` = c.`CATEGID`  and p.PROID='".$_SESSION['gcCart'][$i]['productid']."'";
            $mydb->setQuery($query);
            $cur = $mydb->loadResultList();
            foreach ($cur as $result){
            ?>
            <tr>
              <!-- <td></td> -->
              <td><img src="admin/products/<?php echo $result->IMAGES ?>"  width="50px" height="50px"></td>
              <td><?php echo $result->PRODESC ; ?></td>
              <td align="center"><?php echo $_SESSION['gcCart'][$i]['qty']; ?></td>
              <td>Rp.<?php echo  $result->PRODISPRICE ?></td>
              <td>Rp.<output><?php echo $_SESSION['gcCart'][$i]['price']?></output></td>
            </tr>
            <?php
            $tot +=$_SESSION['gcCart'][$i]['price'];
            }
            }
            }
            ?>
            
          </tbody>
          
        </table>
        <div class="  pull-right">
          <p align="right">
            <div > Total Price :   Rp. <span id="sum">0.00</span></div>
            <div > Delivery Fee : Rp. <span id="fee">0.00</span></div>
            <div> Overall Price : Rp. <span id="overall"><?php echo $tot ;?></span></div>
            <input type="hidden" name="alltot" id="alltot" value="<?php echo $tot ;?>"/>
          </p>
        </div>
        
      </div>
    </div>
  </section>
  
  <section id="do_action">
    <div class="container">
      <div class="heading">
      </p>
    </div>
    <div class="row">
      <div class="row">
        <div class="col-md-7">
          <div class="form-group">
            <label> Metode Pembayaran : </label>
            <div class="radio" >
              <label >
                <input type="radio"  class="paymethod" name="paymethod" id="deliveryfee" value="Cash on Delivery" checked="true" data-toggle="collapse"  data-parent="#accordion" data-target="#collapseOne" >Cash on Delivery
                
              </label>
            </div>
          </div>
          <div class="panel">
            <div class="panel-body">
              <div class="form-group ">
                <label>Address where to deliver</label>
                
                <div class="col-md-12">
                  <label class="col-md-4 control-label" for=
                  "provinsi">Province:</label>
                  <div class="col-md-8">
                    <select class="form-control" id="provinsi" >
                      <option class="hidden" >Select</option>
                    </select>
                  </div>
                </div>
                <div class="col-md-12 mt-5">
                  <label class="col-md-4 control-label" for=
                  "city">City:</label>
                  <div class="col-md-8">
                    <select class="form-control" id="city">
                      <option class="hidden" >Loading ...</option>
                    </select>
                  </div>
                </div>
                <input type="hidden" name="PLACE" id="PLACE">
                
              </div>
              
            </div>
          </div>
          
          <input type="hidden"  placeholder="HH-MM-AM/PM"  id="CLAIMEDDATE" name="CLAIMEDDATE" value="<?php echo date('y-m-d h:i:s') ?>"  class="form-control"/>
        </div>
        
        
        
      </div>
      <br/>
      <div class="row">
        <div class="col-md-6">
          <a href="index.php?q=cart" class="btn btn-default pull-left"><span class="glyphicon glyphicon-arrow-left"></span>&nbsp;<strong>View Cart</strong></a>
        </div>
        <div class="col-md-6">
          <button type="submit" class="btn btn-pup  pull-right " name="btn" id="btn" onclick="return validatedate();"   /> Submit Order <span class="glyphicon glyphicon-chevron-right"></span></button>
        </div>
      </div>
      
    </div>
  </div>
  </section><!--/#do_action-->
</form>
<script type="text/javascript">

function optionProvinsi(array) {
  let output = '';
    $.each(array, function(i,provinsi) {
      output += `<option value='${provinsi.province_id}'> ${provinsi.province} </option>`;
    })
    return output
}
function optionCity(array) {
  let output = '';
    $.each(array, function(i,city) {
      output += `<option value='${city.city_id}'> ${city.city_name} </option>`;
    })
    return output
}

  const CORS = 'https://cors-anywhere.herokuapp.com/'
$.ajax({
  url: CORS+'https://api.rajaongkir.com/starter/province',
  type: 'GET',
  headers: {
  'key': '0e859f70f2a6a110e4a2a60a57b35b3e',
  },
  success:  function(data) {
    $('#provinsi').empty().append(optionProvinsi(data.rajaongkir.results))
}
});
$('#provinsi').on('change', function () {
  const provinsiId = $(this).val()
    $.ajax({
      url: CORS+'https://api.rajaongkir.com/starter/city?province='+ provinsiId,
      type: 'GET',
      headers: {
      'key': '0e859f70f2a6a110e4a2a60a57b35b3e',
      },
      success:  function(data) {
        $('#city').empty().append(optionCity(data.rajaongkir.results))
    }
    });
})
$('#city').on('change', function () {
  cityId = $(this).val()
  $.ajax({
    url: CORS+'https://api.rajaongkir.com/starter/cost',
    type: 'POST',
    headers: {
    'content-type': 'application/x-www-form-urlencoded',
    'key': '0e859f70f2a6a110e4a2a60a57b35b3e',
    },
    data: {
      origin: 278,
      destination: cityId,
      weight: 1000,
      courier: 'jne'
    },
    success:  function(data) {
      const OKE = data.rajaongkir.results[0].costs[0].cost[0].value
      const REG = data.rajaongkir.results[0].costs[1].cost[0].value
      let cost = OKE || REG
      $('#PLACE').val(cost)
      console.log(cost)
      $( ".paymethod" ).trigger( "click" );
  }
  });
})
</script>